<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PBolsistas */

$this->title = 'Update P Bolsistas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'P Bolsistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pbolsistas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

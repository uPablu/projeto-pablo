<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PBolsistasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'P Bolsistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pbolsistas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create P Bolsistas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'situacao',
            'datanasc',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

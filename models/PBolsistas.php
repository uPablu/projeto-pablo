<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "p_bolsistas".
 *
 * @property int $id
 * @property string $nome
 * @property int $situacao
 * @property string $datanasc
 */
class PBolsistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'p_bolsistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['situacao'], 'integer'],
            [['datanasc'], 'safe'],
            [['nome'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'situacao' => 'Situacao',
            'datanasc' => 'Datanasc',
        ];
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PBolsistas */

$this->title = 'Create P Bolsistas';
$this->params['breadcrumbs'][] = ['label' => 'P Bolsistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pbolsistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
